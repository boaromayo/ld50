local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("has_fuschia")
  item:set_amount_savegame_variable("amount_fuschia")
  item:set_brandish_when_picked(false)
  item:set_sound_when_picked("pick")
end

function item:on_obtaining(variant, savegame_variable)
  game:add_score(30)

  if item:get_variant() == 0 then
    item:set_variant(1)
  end
  item:add_amount(1)
end