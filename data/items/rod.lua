local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("has_rod")
  item:set_assignable(true)
  item:set_attack(3)
end

function item:on_obtained(variant, savegame_variable)
  item:set_obtainable(not game:has_item("rod"))

  -- Assign item to first slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    local hero = game:get_hero()
    game:set_item_assigned(slot, item)
    hero:set_attack(item:get_attack())
  end
end

local function enemy_collided(rod, other)
  if other:get_type() ~= "enemy" then
    return
  end
  local enemy = other
  local hero = game:get_hero()
  local reaction = enemy:calculate_damage(hero:get_attack())

  enemy:receive_attack_consequence("sword", reaction)
end

local function is_rock(destructible)
  local sprite = destructible:get_sprite()

  if sprite == nil then
    return false
  end

  local sprite_id = sprite:get_animation_set()
  return sprite_id == "entities/rock" or sprite_id:match("^entities/rock")
end

local function rock_collided(rod, other)
  if other:get_type() ~= "custom_entity" then
    return false
  end

  if not is_rock(other) then
    return false
  end

  local x, y, width, height = rod:get_bounding_box()
  return other:overlaps(x, y, width + 2, height + 2)
end

-- Event called when the hero starts using this item.
function item:on_using()
  -- In this case, hero uses rod.
  local map = item:get_map()
  local hero = map:get_hero()
  local direction = hero:get_direction()
  hero:set_animation("rod")

  local x, y, layer = hero:get_center_position()
  local ox, oy = hero:get_sprite():get_xy()
  local rod = map:create_custom_entity({
    x = x + ox,
    y = y + oy,
    layer = layer,
    width = 16,
    height = 16,
    direction = direction,
    sprite = "hero/rod",
  })

  rod:add_collision_test("sprite", enemy_collided)
  rod:add_collision_test("sprite", rock_collided)

  -- Fire a fireball.
  sol.audio.play_sound("fireball")
  item:fire()

  sol.timer.start(rod, 10, function()
    rod:set_position(hero:get_position())
    return true -- Loop around.
  end)

  sol.timer.start(hero, 300, function()
    rod:remove()
    hero:unfreeze()
    item:set_finished()
  end)
end

function item:fire()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, layer = hero:get_position()
  local direction = hero:get_direction()
  local fire_width, fire_height = 8, 8

  local fireball = map:create_custom_entity({
    model = "fireball",
    x = x,
    y = y + 2,
    layer = layer,
    width = fire_width,
    height = fire_height,
    direction = direction,
  })

  local movement = sol.movement.create("straight")
  local speed = 160
  local angle = direction * math.pi / 2
  movement:set_speed(speed)
  movement:set_angle(angle)
  movement:set_smooth(false)
  movement:start(fireball)
end

local function start_enemy_reactions()

  local enemy_meta = sol.main.get_metatable("enemy")
  
  if enemy_meta.get_fire_reaction ~= nil then
    return
  end

  enemy_meta.fire_reaction = 1
  enemy_meta.fire_reaction_sprite = {}

  function enemy_meta:get_fire_reaction(sprite)
    
    local enemy = self
    if sprite ~= nil and enemy.fire_reaction_sprite[sprite] ~= nil then
      return enemy.fire_reaction_sprite[sprite]
    end

    return enemy.fire_reaction
  end

  function enemy_meta:set_fire_reaction(reaction)
    
    local enemy = self
    enemy.fire_reaction = reaction
  end

  function enemy_meta:set_fire_reaction_sprite(sprite, reaction)

    local enemy = self
    enemy.fire_reaction_sprite[sprite] = reaction
  end

end

start_enemy_reactions()
