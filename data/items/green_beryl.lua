local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("has_green_beryl")
  item:set_amount_savegame_variable("amount_green_beryl")
  item:set_brandish_when_picked(false)
  item:set_sound_when_picked("pick")
end

function item:on_obtaining()
  game:add_score(50)

  if item:get_variant() == 0 then
    item:set_variant(1)
  end
  item:add_amount(1)
end