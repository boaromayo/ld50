local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("has_paper")
  item:set_brandish_when_picked(false)
end

-- Event called when the hero starts using this item.
function item:on_using()

  -- Call tutorial again.
  game:start_dialog("_map.start.scrap.2", function()
    game:start_dialog("_map.start.1")
  end)

  item:set_finished()
end
