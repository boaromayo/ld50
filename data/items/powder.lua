local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("has_powder")
  item:set_amount_savegame_variable("amount_powder")
  item:set_brandish_when_picked(false)
  item:set_sound_when_picked("treasure")
end

function item:on_obtaining()
  local time_extend = math.random(40, 100)
  game:add_countdown_time(time_extend)
  item:add_amount(1)
end