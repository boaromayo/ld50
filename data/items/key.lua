local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()

  item:set_brandish_when_picked(false)
end

-- Event called when the hero starts using this item.
function item:on_obtaining()
  game:add_key()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  pickable:create_sprite("entities/items", "key")
end
