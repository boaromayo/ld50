-- Score counter.

local score_builder = {}

function score_builder:new(game, config)

  local score = {}

  -- Set up timer.
  score.dst_x, score.dst_y = config.x, config.y
  score.surface = sol.surface.create(44, 16)
  score.text = sol.text_surface.create({
    font = "square_digits",
    horizontal_alignment = "right"
  })
  score.text:set_text(game:get_score())
  score.icon = sol.surface.create("hud/misc-ld50.png")
  score.displayed_score = game:get_score()

  function score:on_started()
    score:check()
    score:rebuild_surface()
  end

  function score:check()
    
    local need_update = false
    local current_score = game:get_score()
  
    -- Check if the score is set to the current score.
    if current_score ~= score.displayed_score then
      need_update = true
      -- Increase incrementally.
      local increment = 0
      local difference = 0
      if current_score > score.displayed_score then
        -- Change amount to increment depending on the difference.
        difference = math.abs(current_score - score.displayed_score)
        if difference > 20 then
          increment = 5
        else
          increment = 1
        end
      else
        increment = -1
      end
      score.displayed_score = score.displayed_score + increment


      -- Play sound when score updated for every 3 values.
      if score.displayed_score % 3 == 0 then
        sol.audio.play_sound("picked_money")
      end
    end

    if need_update then
      score:rebuild_surface()
    end

    -- Update every 1/20 of a second.
    sol.timer.start(score, 50, function()
      score:check()
    end)
  end

  function score:rebuild_surface()
    
    score.surface:clear()

    -- Draw icon.
    score.icon:draw_region(32, 0, 8, 8, score.surface, 0, 4)

    -- Draw the current time remaining.
    score.text:set_text(score.displayed_score)
    score.text:draw(score.surface, 40, 8)
  end
  
  
  function score:on_draw(surface)

    local x, y = score.dst_x, score.dst_y
    local width, height = surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    score.surface:draw(surface, x, y)
  end

  return score
end

return score_builder