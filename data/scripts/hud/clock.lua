-- Clock countdown timer.

local clock_builder = {}

function clock_builder:new(game, config)

  local clock = {}

  -- Set up timer.
  clock.dst_x, clock.dst_y = config.x, config.y
  clock.surface = sol.surface.create(48, 16)
  clock.text = sol.text_surface.create({
    font = "square_digits",
    horizontal_alignment = "left"
  })
  clock.icon = sol.surface.create("hud/misc-ld50.png")
  clock.displayed_time = game:get_countdown_time()

  function clock:on_started()
    clock:check()
    clock:rebuild_surface()
  end

  function clock:check()
    
    local need_update = false
    local current_time = game:get_countdown_time()
  
    -- Check if the displayed time is set to the current time.
    if current_time ~= clock.displayed_time then
      need_update = true
      -- Increase incrementally.
      local increment = 0
      if current_time > clock.displayed_time then
        increment = 1
      else
        increment = -1
      end
      clock.displayed_time = clock.displayed_time + increment

      -- Start turning yellow if displayed time is below 100.
      if clock.displayed_time <= 100 then
        clock.text:set_font("square_yellow_digits")
      else
        clock.text:set_font("square_digits")
      end

      -- Start game over if time runs out.
      if clock.displayed_time <= 0 then
        game:start_game_over()
      end
    end

    if need_update then
      clock:rebuild_surface()
    end

    -- Update every 1/20 of a second.
    sol.timer.start(clock, 50, function()
      clock:check()
    end)
  end

  function clock:rebuild_surface()
    
    clock.surface:clear()

    -- Draw icon.
    clock.icon:draw_region(0, 0, 16, 16, clock.surface, 0, 0)

    -- Draw the current time remaining. 
    -- Leave filler zeros if below a hundred.
    clock.text:set_text(string.format("%03d", clock.displayed_time))
    clock.text:draw(clock.surface, 16, 8)
  end
  
  
  function clock:on_draw(surface)

    local x, y = clock.dst_x, clock.dst_y
    local width, height = surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    clock.surface:draw(surface, x, y)
  end

  return clock
end

return clock_builder