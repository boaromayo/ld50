local health_builder = {}

function health_builder:new(game, config)

  local health = {}
  local heart_width = 12
  local heart_height = 12

  if config ~= nil then
    health.dst_x, health.dst_y = config.x, config.y
  end

  health.surface = sol.surface.create(48, heart_height)
  health.heart_border_sprite = sol.sprite.create("hud/empty_heart")
  health.heart_image = sol.surface.create("hud/hearts-ld50.png")
  health.text = sol.text_surface.create({
    font = "square_digits",
    horizontal_alignment = "left",
  })
  health.transparent = false
  health.current_life = game:get_life()
  health.max_life = game:get_max_life()

  function health:on_started()
    health:check()
    health:rebuild_surface()
  end

  function health:check()
    local need_update = false
    local life = game:get_life()

    if life ~= health.current_life then
      need_update = true
      local increment = 0
      if life > health.current_life then
        increment = 1
      else
        increment = -1
      end

      if increment ~= 0 then
        health.current_life = health.current_life + increment
      end
    end

    -- Check if health is low.
    need_update = health:check_for_alert(need_update)

    -- Set text.
    health.text:set_text(health.current_life)

    -- Redraw if something has changed.
    if need_update then
      health:rebuild_surface()
    end

    -- Make next check.
    sol.timer.start(self, 50, function()
      health:check()
    end)
  end

  -- Check if health is below a quarter.
  function health:check_for_alert(need_update)

    if health.current_life <= health.max_life / 4 then
      need_update = true
      if health.heart_border_sprite:get_animation() ~= "danger" then
        health.heart_border_sprite:set_animation("danger")
        health.text:set_font("square_yellow_digits")
      end
    else
      if health.heart_border_sprite:get_animation() ~= "normal" then
        need_update = true
        health.heart_border_sprite:set_animation("normal")
        health.text:set_font("square_digits")
      end
    end

    return need_update
  end

  function health:rebuild_surface()
    health.surface:clear()

    -- Draw heart and current health.
    local x, y = 0, 0
    local life_ratio = math.floor(health.current_life * heart_height / health.max_life)
    health.heart_border_sprite:draw(health.surface, x, y)
    y = heart_height - life_ratio
    health.heart_image:draw_region(0, heart_height - life_ratio, heart_width, life_ratio, health.surface, x, y)
    y = 0
    health.text:draw(health.surface, x + 16, y + 6)
  end

  function health:set_dst_position(x, y)
    health.dst_x = x
    health.dst_y = y
  end

  function health:get_surface()
    return health.surface
  end

  function health:on_draw(surface)
    
    local x, y = health.dst_x, health.dst_y
    local width, height = surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    health.surface:set_opacity(health.transparent and 128 or 255)
    health.surface:draw(surface, x, y)
  end

  function health:set_transparent(transparent)
    if health.transparent ~= transparent then
      health.transparent = transparent
    end
  end

  health:rebuild_surface()

  return health
end

return health_builder