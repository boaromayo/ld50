-- Gems collection.

local gems_box_builder = {}

function gems_box_builder:new(game, config)

  local gems_box = {}

  -- Gem names to load items.
  local gem_names = {
    "sapphire",
    "ruby",
    "emerald",
    "fuschia",
    "crystal",
    "pearl",
    "pyramid",
    "green_beryl",
    "topaz",
    "vulkstone"
  }

  local rows = 1
  local columns = #gem_names -- Length of table in gem names.
  local grid_size = 20
  local spacing = 4
  local box_width = columns * (grid_size + spacing) - spacing
  local box_height = rows * (grid_size + spacing) - spacing

  gems_box.dst_x, gems_box.dst_y = config.x, config.y
  gems_box.surface = sol.surface.create(box_width, box_height)
  gems_box.grid_surface = sol.surface.create(grid_size, grid_size)
  gems_box.gem_sprite = sol.sprite.create("entities/items")
  gems_box.amount_text = sol.text_surface.create({
    font = "square_digits",
    horizontal_alignment = "right",
    vertical_alignment = "bottom",
  })
  gems_box.grid_fill = { 40, 40, 24 }
  gems_box.grid_opacity = 128
  gems_box.opacity = 255
  gems_box.transparent = false
  gems_box.gem_sprites = {}
  gems_box.amounts_displayed = {}
  
  -- Set grid transparency and color.    
  gems_box.grid_surface:fill_color(gems_box.grid_fill)
  gems_box.grid_surface:set_opacity(gems_box.grid_opacity)
 
  function gems_box:on_started()
    gems_box:build_background()
    gems_box:build_gems()
    gems_box:build_gem_amounts(gems_box.amounts_displayed)
    gems_box:check()
  end

  function gems_box:build_gem_amounts(gem_amounts)
    for _, name in ipairs(gem_names) do
      gem_amounts[name] = nil      
    end
  end

  function gems_box:build_background()

    -- No need for a y-coordinate, since this will be one row.
    -- Draw out slots per column.
    local dest_x = 0
    for col = 1, columns do
      gems_box.grid_surface:draw(gems_box.surface, dest_x, 0)
      dest_x = dest_x + grid_size + spacing
    end
  end

  function gems_box:build_gems()
    
    local gem_sprite = gems_box.gem_sprite

    -- Check every gem.
    for index, gem_name in ipairs(gem_names) do
      local gem = game:get_item(gem_name)
      local variant = gem:get_variant()

      -- Put gem sprites into table for later drawing.
      if variant > 0 then
        gem_sprite:set_animation(gem_name)
        gem_sprite:set_direction(variant - 1)
        gems_box.gem_sprites[gem_name] = gem_sprite

        -- Update gem amounts if player has them.
        if gem:has_amount() then
          local amount = gem:get_amount()
          if gems_box.amounts_displayed[gem_name] ~= amount then
            gems_box.amounts_displayed[gem_name] = amount
          end
        end

      end
    end
  end

  function gems_box:check()

    local need_update = false

    for _, gem_name in ipairs(gems_box.amounts_displayed) do

      if gems_box.gem_sprites[gem_name] ~= nil then
        need_update = true
      end
      if gems_box.amounts_displayed[gem_name] ~= nil then
        need_update = true
      end
    end

    if need_update then
      gems_box:rebuild_surface()
    end

    -- Schedule next check.
    sol.timer.start(gems_box, 50, function()
      gems_box:check()
    end)
  end

  function gems_box:rebuild_surface()
    gems_box.surface:clear()

    gems_box:build_background()
    gems_box:build_gems()

    -- Set transparency.
    if gems_box.transparent then
      gems_box.opacity = gems_box.grid_opacity
      gems_box.grid_opacity = math.floor(gems_box.grid_opacity / 2)
    else
      gems_box.grid_opacity = gems_box.opacity
      gems_box.opacity = 255
    end
    gems_box.surface:set_opacity(gems_box.opacity)

    -- Set up gem drawing.
    local dest_x, dest_y = 0, 0
    local margin = 2

    -- Draw gems.
    for index, gem_name in ipairs(gems_box.amounts_displayed) do
      local origin_x, origin_y = game:get_item(gem_name):get_origin()

      -- Draw gem sprites.
      gems_box.gem_sprites[gem_name]:draw(gems_box.surface, 
        dest_x + origin_x + margin,
        dest_y + origin_y + margin)

      -- Draw gem amounts.
      gems_box.amount_text:set_text(gems_box.amounts_displayed[gem_name])
      print(gems_box.amounts_displayed[gem_name])
      gems_box.amount_text:draw(gems_box.surface, 
        dest_x + grid_size,
        dest_y + grid_size)

      if index % columns == 0 then
        dest_y = dest_y + grid_size + spacing
        dest_x = 0
      else
        dest_x = dest_x + grid_size + spacing
      end
    end
  end

  function gems_box:on_draw(surface)

    local x, y = gems_box.dst_x, gems_box.dst_y
    local width, height = surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    gems_box.surface:draw(surface, x, y) 
  end

  function gems_box:set_transparent(transparent)
    if gems_box.transparent ~= transparent then
      gems_box.transparent = transparent
    end
  end

  function gems_box:is_transparent()
    return gems_box.transparent
  end

  return gems_box
end

return gems_box_builder