-- Temple key indicator.

local key_indicator_builder = {}

function key_indicator_builder:new(game, config)
  
  local key = {}

  -- Set up key indicator.
  key.dst_x, key.dst_y = config.x, config.y
  key.surface = sol.surface.create(16, 16)
  key.icon = sol.surface.create("hud/key_icon-ld50.png")
  key.visible = false

  function key:on_started()
    -- Check for key icon.
    sol.timer.start(key, 50, function()
      key:check()
      return true
    end)
  end

  function key:check()

    local need_update = false

    -- Keys are not available in this map, get out of function.
    if not game:key_enabled() then
      return true
    end

    local visible = game:has_key()
    if visible ~= key.visible then
      key.visible = visible
      need_update = true
    end

    if need_update then
      key:rebuild_surface()
    end
  end

  -- Draw key if player has it for this floor.
  function key:rebuild_surface()

    key.surface:clear()

    -- Draw key icon.
    if key.visible then
      key.icon:draw(key.surface)
    end
  end

  -- Draw key indicator to screen.
  function key:on_draw(surface)

    local x, y = key.dst_x, key.dst_y
    local width, height = surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    key.surface:draw(surface, x, y)
  end

  return key
end

return key_indicator_builder