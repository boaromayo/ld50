-- Defines the elements to put in the HUD
-- and their position on the game screen.

-- You can edit this file to add, remove or move some elements of the HUD.

-- Each HUD element script must provide a method new()
-- that creates the element as a menu.
-- See for example scripts/hud/hearts.

-- Negative x or y coordinates mean to measure from the right or bottom
-- of the screen, respectively.

local hud_config = {

  -- Health counter.
  {
    menu_script = "scripts/hud/health",
    x = 8,
    y = 8,
  },

  -- Clock counter.
  {
    menu_script = "scripts/hud/clock",
    x = -48,
    y = 8,
  },

  -- Key indicator.
  {
    menu_script = "scripts/hud/key",
    x = 2,
    y = -36,
  },

  -- Score counter.
  {
    menu_script = "scripts/hud/score",
    x = 8,
    y = -20,
  }
}

return hud_config
