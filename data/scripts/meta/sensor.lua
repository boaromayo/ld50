local sensor_meta = sol.main.get_metatable("sensor")

local victory_screen_builder = require("scripts/menus/victory")

function sensor_meta:on_activated()
  local sensor = self
  local name = sensor:get_name()
  local map = sensor:get_map()
  local hero = map:get_hero()

  -- When no name found for sensor, get out of event. This is to prevent
  -- errors from printing.
  if name == nil then
    return
  end

  -- When player touches sensor, start close entrance doors processing.
  if name:match("^sensor_entrance_lock") then
    local doors = map:get_entities("door_entrance")

    -- Close doors behind player.
    hero:freeze()
    for door in doors do
      if door:is_open() then
        map:close_doors(door:get_name())
      end
    end
    sol.timer.start(map, 1000, function()
      hero:unfreeze()
    end)
    sensor:remove()
  end

  -- Set this sensor as solid ground to return to.
  if sensor:get_property("save_solid_ground") or name:match("^sensor_save_solid_ground") then
    hero:save_solid_ground() 
  end

  -- Set this sensor as solid ground to reset.
  if sensor:get_property("reset_solid_ground") or name:match("^sensor_reset_solid_ground") then
    hero:reset_solid_ground()
  end

  -- Go to victory screen.
  if name:match("^sensor_victory") then
    hero:freeze()
    local game = map:get_game()
    local victory_screen = victory_screen_builder:new(game)
    sol.menu.start(map, victory_screen)
  end
end