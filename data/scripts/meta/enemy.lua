local enemy_meta = sol.main.get_metatable("enemy")

function enemy_meta:calculate_damage(attack)
  local enemy = self

  if type(attack) == "number" then
    enemy:hurt(attack)
  end
end

-- Helper function to classify enemy reaction to player's attacks.
function enemy_meta:receive_attack_consequence(attack_type, reaction)
  local enemy = self

  if type(reaction) == "number" then
    enemy:calculate_damage(reaction)
  elseif reaction == "custom" then
    if enemy.on_custom_attack_received ~= nil then
      enemy:on_custom_attack_received(attack_type)
    end
  end
end

return true