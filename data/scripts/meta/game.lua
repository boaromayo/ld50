require("scripts/multi_events")
local game_meta = sol.main.get_metatable("game")

-- Enable player to use attack button to call rod.
game_meta:register_event("on_command_pressed", function(game, command)
  if command == "attack" then
    local slot = 1 -- Rod used
    return game:item_used(slot)
  elseif command == "item_1" then
    return true -- Workaround to disable item_1 command.
  else
    return false
  end
end)

function game_meta:item_used(slot)
  local game = self
  local item = game:get_item_assigned(slot)
  if item then
    if game:is_paused() then
      return true
    end
    item:on_using()
    return true
  end
  return false
end

-- Start running time here.
function game_meta:run_countdown(start_time)
  local game = self
  local START_TIME = start_time or 500
  game:set_value("countdown_timer", START_TIME * 1000)
  game:start_countdown()
  local timer = sol.timer.start(game, 100, function()    
    local time = game:get_value("countdown_timer")
    time = time - 100
    game:set_value("countdown_timer", time)
    return time > 0 and game.enable_countdown -- Repeat the timer.
  end)
  timer:set_suspended_with_map(true)
end

function game_meta:set_countdown_time(time)
  local game = self
  game:set_value("countdown_timer", time)
end

function game_meta:get_countdown_time()
  local game = self
  local current_time_millisecs = game:get_value("countdown_timer")
  local current_time = math.floor(current_time_millisecs / 1000)
  return current_time
end

function game_meta:add_countdown_time(time)
  local game = self
  local time_millis = time * 1000
  local new_time = game:get_value("countdown_timer") + time_millis
  game:set_countdown_time(new_time)
end
  
function game_meta:take_countdown_time(time)
  local game = self
  local time_millis = time * 1000
  local new_time = game:get_value("countdown_timer") - time_millis
  if new_time < 0 then
    game:set_countdown_time(0)
  else
    game:set_countdown_time(new_time)
  end
end

function game_meta:start_countdown()
  local game = self
  game.enable_countdown = true
end

function game_meta:stop_countdown()
  local game = self
  if game.enable_countdown then
    game.enable_countdown = false
  end
end

-- Scoring system here.
function game_meta:set_score(score)
  local game = self
  game:set_value("score", score)
end

function game_meta:get_score()
  local game = self
  return game:get_value("score")
end

function game_meta:add_score(score)
  local game = self
  local new_score = game:get_score() + score
  game:set_score(new_score)
end

function game_meta:take_score(score)
  local game = self
  local new_score = game:get_score() - score
  if new_score < 0 then
    game:set_score(0)
  else
    game:set_score(new_score)
  end
end

function game_meta:get_key_savegame_variable()
  local game = self
  local map = game:get_map()

  if map ~= nil then
    if map.key_savegame_variable ~= nil then
      return map.key_savegame_variable
    end
  else
    return nil
  end
end

function game_meta:key_enabled()
  local game = self
  return game:get_key_savegame_variable() ~= nil
end

function game_meta:get_key()
  local game = self
  if not game:key_enabled() then
    error("Key does not exist on current map", 2)
  end

  return game:get_value(game:get_key_savegame_variable()) or false
end

function game_meta:has_key()
  local game = self
  return game:get_key() == true
end

function game_meta:add_key()
  local game = self
  if game:key_enabled() then
    game:set_value(game:get_key_savegame_variable(), true)
  else
    error("Key does not exist on current map")
  end
end

function game_meta:take_key()
  local game = self
  if game:has_key() then
    game:set_value(game:get_key_savegame_variable(), false)
  else
    error("No key on hand to take")
  end
end

return true