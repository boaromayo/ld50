require("scripts/multi_events")
local hero_meta = sol.main.get_metatable("hero")

hero_meta:register_event("on_created", function(hero)
  --local default_stats = {}
  hero:set_tunic_sprite_id("hero/player")
  --hero:set_properties(default_stats)
end)

hero_meta:register_event("on_state_changed", function(hero, state)
  -- TODO: Make hero state sound changes here.
end)

function hero_meta:set_attack(atk)
  local hero = self
  hero:set_property("attack", atk)
end

function hero_meta:get_attack()
  local hero = self
  return hero:get_property("attack")
end

function hero_meta:set_defense(def)
  local hero = self
  hero:set_property("defense", def)
end

function hero_meta:get_defense()
  local hero = self
  return hero:get_property("defense")
end

function hero_meta:add_attack(atk)
  local hero = self
  local new_atk = hero:get_attack() + atk
  hero:set_attack(new_atk)
end

function hero_meta:take_attack(atk)
  local hero = self
  local new_atk = hero:get_attack() - atk
  if new_atk < 0 then
    new_atk = 0
  end
  hero:set_attack(new_atk)
end

function hero_meta:add_defense(def)
  local hero = self
  local new_def = hero:get_defense() + def
  hero:set_defense(new_def)
end

function hero_meta:take_defense(def)
  local hero = self
  local new_def = hero:get_defense() - def
  if new_def < 0 then
    new_def = 0
  end
  hero:set_defense(new_def)
end