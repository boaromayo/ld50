local item_meta = sol.main.get_metatable("item")

function item_meta:set_attack(atk)
  local item = self
  item.attack = atk or 0
end

function item_meta:get_attack()
  local item = self
  return item.attack or 0
end

return true