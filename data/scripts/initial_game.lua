local initial_game = {}

function initial_game:initialize_new_savegame(game)

  local entrance = math.random(1,7) -- Set random entrance, bad idea hard-coding it, tho.
  game:set_starting_location("ld50/1f", "entrance_" .. entrance)  -- Starting location.
  
  game:set_max_life(10)
  game:set_life(game:get_max_life())
  game:set_max_money(100)
  game:set_score(0)
  game:run_countdown() -- Start countdown.
end

return initial_game
