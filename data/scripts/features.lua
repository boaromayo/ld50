require("scripts/meta/game")
require("scripts/meta/hero")
require("scripts/meta/enemy")
require("scripts/meta/item")
require("scripts/meta/sensor")
require("scripts/hud/hud")
require("scripts/menus/game_over")
require("scripts/menus/pause_menu")
require("scripts/menus/dialog_box")

return true
