-- Math utility functions.
local math_tools = {}

function math_tools.clamp(value, min, max)
  return (value < min and min or value) > max and max or value
end

function math_tools.round_angle(angle)
  return math.floor((angle + math.pi / 4) / math.pi / 2)) % 4
end

return math_tools