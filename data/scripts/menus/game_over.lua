require("scripts/multi_events")
local language_manager = require("scripts/language_manager")

local function setup_gameover(game)

  if game.game_over_menu ~= nil then
    return
  end

  local gameover_menu = {}
  game.game_over_menu = gameover_menu

  game:register_event("on_game_over_started", function(game)
    sol.menu.start(game:get_map(), gameover_menu)
  end)

  function gameover_menu:on_started()
    self.mode = 1
    self.cursor_position = 1
    self.allow_input = false

    self.surface = sol.surface.create(sol.video.get_quest_size())

    self.background = sol.surface.create(sol.video.get_quest_size())
    self.cursor = sol.sprite.create("menus/gameover_cursor")
    self.confirm_button = sol.sprite.create("menus/gameover_signs")
    self.quit_button = sol.sprite.create("menus/gameover_signs")

    local font, font_size = language_manager:get_dialog_font()
    local score_font = "square_digits"
    local black_color = {0, 0, 0}
    local white_color = {255, 255, 255}
    local alignment = "center"

    self.background:fill_color(black_color)
    self.background:set_opacity(0)

    self.par_1_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = white_color,
      horizontal_alignment = alignment,
      vertical_alignment = "top",
    })
    self.par_2_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = white_color,
      horizontal_alignment = alignment,
      vertical_alignment = "middle",
    })
    self.question_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = white_color,
      horizontal_alignment = alignment,
      vertical_alignment = "bottom",
    })

    self.score_display_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = white_color,
      horizontal_alignment = alignment,
    })
    self.score_text = sol.text_surface.create({
      font = score_font,
      color = white_color,
      horizontal_alignment = alignment,
    })

    self.confirm_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })
    self.quit_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })

    self:hide_components()
    self:set_text()

    self.confirm_button:set_animation("confirm")
    self.quit_button:set_animation("quit")

    sol.timer.start(self, 1000, function()
      self.surface:fade_in(30)
      self:start()
    end)
  end

  function gameover_menu:hide_components()
    self.par_1_text:set_opacity(0)
    self.par_2_text:set_opacity(0)
    self.question_text:set_opacity(0)

    self.score_display_text:set_opacity(0)
    self.score_text:set_opacity(0)

    self.confirm_button:set_opacity(0)
    self.confirm_text:set_opacity(0)
    self.quit_button:set_opacity(0)
    self.quit_text:set_opacity(0)

    self.cursor:set_opacity(0)
  end

  function gameover_menu:set_text()

    self.par_1_text:set_text_key("gameover.1")
    
    if game:has_item("rod") then
      self.par_2_text:set_text_key("gameover.2")
      self.question_text:set_text_key("gameover.question2")
    else
      self.question_text:set_text_key("gameover.question")
    end

    self.score_display_text:set_text("Score: ")
    self.score_text:set_text(string.format("%3d", game:get_score()))

    self.confirm_text:set_text_key("gameover.yes")
    self.quit_text:set_text_key("gameover.no")
  end

  local function fadein_img(img, time)
    img:fade_in(time)
  end

  function gameover_menu:start()
    self.mode = 2
    
    game:set_hud_enabled(false)
    fadein_img(self.background, 30)

    sol.timer.start(self, 1000, function()
      fadein_img(self.par_1_text, 60)
      sol.timer.start(self, 2000, function()
        fadein_img(self.par_2_text, 60)
        sol.timer.start(self, 3000, function()
          fadein_img(self.question_text, 60)
        end)
      end)
    end)
    
    sol.timer.start(self, 8000, function()
      fadein_img(self.score_display_text, 30)
      fadein_img(self.score_text, 30)
      sol.timer.start(self, 2000, function() 
        self.confirm_button:set_opacity(255)
        self.quit_button:set_opacity(255)
        self.confirm_text:set_opacity(255)
        self.quit_text:set_opacity(255)

        self.cursor:set_opacity(255)
        self.allow_input = true
      end)
    end)

    self.cursor:set_animation("select")
  end

  function gameover_menu:branch_choice()

    sol.audio.play_sound("confirm")
    self.cursor:set_animation("stopped")
    
    if self.cursor_position == 1 then
      self.confirm_button:set_animation("confirm_accept")
      sol.timer.start(self, 1000, function()
        sol.main.reset()
      end)
    elseif self.cursor_position == 2 then
      self.quit_button:set_animation("quit_accept")
      sol.timer.start(self, 1000, function()
        sol.main.exit()
      end)
    end
  end

  function gameover_menu:on_draw(surface)

    local width, height = surface:get_size()

    self.surface:clear()

    self.background:draw(self.surface)

    if self.mode == 2 then
      self:draw_text()
      self:draw_score(width, height)
      self:draw_choices(width, height)
      self:draw_cursor()
    end

    self.surface:draw(surface, width / 2 - 160, height / 2 - 120)
  end

  function gameover_menu:draw_text()
    self.par_1_text:draw(self.surface, 160, 40)
    self.par_2_text:draw(self.surface, 160, 80)
    self.question_text:draw(self.surface, 160, 120)
  end

  function gameover_menu:draw_score(width, height)
    self.score_display_text:draw(self.surface, width / 2 - 20, height - 100)
    self.score_text:draw(self.surface, width / 2 + 20, height - 100)
  end

  function gameover_menu:draw_choices(width, height)
    self.confirm_button:draw(self.surface)
    self.quit_button:draw(self.surface)

    self.confirm_button:set_xy(width / 2 - 60, height - 40)
    self.quit_button:set_xy(width / 2 + 60, height - 40)

    self.confirm_text:draw(self.surface, width / 2 - 60, height - 50)
    self.quit_text:draw(self.surface, width / 2 + 60, height - 50)
  end

  function gameover_menu:draw_cursor()
    self.cursor:draw(self.surface)
    self:set_cursor(self.cursor_position)
  end

  function gameover_menu:set_cursor(position)
    self.cursor_position = position
    self:move_cursor()
  end

  function gameover_menu:move_cursor()
    local x, y = 0, 0
    local y_offset = 4
    if self.cursor_position == 1 then
      x, y = self.confirm_button:get_xy()
    else
      x, y = self.quit_button:get_xy()
    end
    self.cursor:set_xy(x, y + y_offset)
  end

  function gameover_menu:on_command_pressed(command)

    local handled = false

    if self.allow_input then
      if command == "up" or command == "down" or 
          command == "left" or command == "right" then
        sol.audio.play_sound("cursor")
        self:set_cursor(3 - self.cursor_position)
        handled = true
      elseif command == "action" or command == "attack" then
        self:branch_choice()
        handled = true
      end
    else
      handled = true
    end

    return handled
  end
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", setup_gameover)

return true