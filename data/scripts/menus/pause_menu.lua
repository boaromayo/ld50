require("scripts/multi_events")
local language_manager = require("scripts/language_manager")

local function setup_pause_menu(game)

  if game.pause_menu ~= nil then
    return
  end

  local pause_menu = {}
  game.pause_menu = pause_menu

  local gem_names = {
    "sapphire",
    "ruby",
    "emerald",
    "fuschia",
    "crystal",
    "pearl",
    "pyramid",
    "green_beryl",
    "topaz",
    "vulkstone"
  }

  local misc_names = {
    "rod",
    "paper"
  }

  local font, font_size = language_manager:get_menu_font()
  local amount_font = "square_digits"
  local white_color = { 255, 255, 255 }
  local gray_color = { 40, 40, 25 }
  local opacity = 192

  local menu_x, menu_y = 30, 40
  local quest_width, quest_height = sol.video.get_quest_size()
  local menu_width, menu_height = quest_width - (menu_x * 2), quest_height - (menu_y * 2)
  local caption_x = menu_width / 2 - menu_x * 2
  local caption_y = menu_height / 2 + (menu_y / 2)
  local grid_size = 20
  local gem_rows = 2
  local gem_columns = 5
  
  pause_menu.cursor = sol.sprite.create("menus/cursor")
  local cursor_x, cursor_y = 0, 0
  local cursor_col, cursor_row = 0, 0

  local caption_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    horizontal_alignment = "center",
    vertical_alignment = "top",
  })
  local caption_key = nil

  local margin = 2
  local spacing = 8

  local grid_box = sol.surface.create(grid_size, grid_size)
  grid_box:fill_color(gray_color)
  grid_box:set_opacity(opacity)

  local item_sprite = sol.sprite.create("entities/items")
  
  local function update_caption()
    
    local index = cursor_row * gem_columns + cursor_col + 1
    local item_name = nil

    if cursor_row == 0 then
      if cursor_col < gem_columns then
        item_name = gem_names[index]
        caption_key = "items.gems." .. item_name
      elseif cursor_col >= gem_columns then
        item_name = misc_names[index - gem_columns]
        caption_key = "items.misc." .. item_name
      end
    elseif cursor_row == 1 then
      item_name = gem_names[index]
      caption_key = "items.gems." .. item_name
    end

    if not game:has_item(item_name) then
      caption_key = nil
    end

    if game:has_item(item_name) then
      caption_text:set_text_key(caption_key)
    else
      caption_text:set_text()
    end
  end

  local function update_cursor()

    local grid_offset = grid_size + spacing
    local tiny_offset = spacing - margin
    -- Set cursor to specific location on item grid.
    cursor_x = (grid_size + spacing - margin * 2) + (cursor_col * grid_offset)
    cursor_y = (grid_size + spacing + tiny_offset) + (cursor_row * grid_offset)

    update_caption()
  end

  local function move_cursor(col, row)

    sol.audio.play_sound("cursor")
    if col ~= 0 then
      if cursor_row == 0 then
        -- First gem row has same row as misc items.
        cursor_col = (cursor_col + col) % (gem_columns + 2)
      else
        cursor_col = (cursor_col + col) % gem_columns
      end
    end
    if row ~= 0 then
      cursor_row = (cursor_row + row) % gem_rows
      -- Fix cursor if column index is too high when on a certain row.
      if cursor_row == 1 and cursor_col > gem_columns - 1 then
        cursor_col = gem_columns - 1
      end
    end

    update_cursor()
  end

  local function display_description()
    -- Get cursor index, and determine a dialog should be played
    local index = cursor_row * gem_columns + cursor_col + 1
    local description_dialog_key = nil
    
    if index > gem_columns and index <= gem_columns + 2 and cursor_row == 0 then
      local increment = index - gem_columns
      if game:has_item(misc_names[increment]) then
        description_dialog_key = "_map.read." .. tostring(increment)
      end
    else
      description_dialog_key = nil
    end

    if description_dialog_key ~= nil then
      game:start_dialog(description_dialog_key)
    end
  end

  local function build_grid_background(surface, game)
    local dest_x, dest_y = 0, 0
    
    for row = 0, gem_rows do
      for col = 0, gem_columns do
        grid_box:draw(surface, dest_x, dest_y)
        dest_x = dest_x + grid_size + spacing
      end

      dest_x = 0
      dest_y = dest_y + grid_size + spacing
    end
  end

  local function build_gems(surface, game)
    local dest_x, dest_y = 0, 0
    local amount_text = sol.text_surface.create({
      font = amount_font,
      horizontal_alignment = "right",
      vertical_alignment = "bottom",
    })

    for index, gem_name in pairs(gem_names) do
      local item = game:get_item(gem_name)
      local variant = item:get_variant()
      local amount = item:get_amount()

      if variant > 0 and amount > 0 then
        local origin_x, origin_y = item_sprite:get_origin()

        item_sprite:set_animation(gem_name)
        item_sprite:get_direction(variant - 1)
        item_sprite:draw(surface, dest_x + origin_x + margin, 
          dest_y + origin_y + margin)

        amount_text:set_text(amount)
        amount_text:draw(surface, dest_x + grid_size, dest_y + grid_size)
      end

      if index % gem_columns == 0 then
        dest_x = 0
        dest_y = dest_y + grid_size + spacing
      else
        dest_x = dest_x + grid_size + spacing
      end
    end
  end

  local function build_misc(surface, game)
    local dest_x, dest_y = 0, 0

    -- Draw background box for rod and paper.
    grid_box:draw(surface, dest_x, dest_y)
    
    local rod_name = misc_names[1]
    local paper_name = misc_names[2]

    local item_rod = game:get_item(rod_name)
    local rod_variant = item_rod:get_variant()

    if rod_variant > 0 then
      local origin_x, origin_y = item_sprite:get_origin()
      
      item_sprite:set_animation(rod_name)
      item_sprite:set_direction(rod_variant - 1)
      item_sprite:draw(surface, dest_x + origin_x + margin,
        dest_y + origin_y + margin)
    end

    dest_x = dest_x + grid_size + spacing

    -- Draw second box for paper.
    grid_box:draw(surface, dest_x, dest_y)

    local item_paper = game:get_item(paper_name)
    local paper_variant = item_paper:get_variant()

    if paper_variant > 0 then
      local origin_x, origin_y = item_sprite:get_origin()
      
      item_sprite:set_animation(paper_name)
      item_sprite:set_direction(paper_variant - 1)
      item_sprite:draw(surface, dest_x + origin_x + margin,
        dest_y + origin_y + margin)
    end
  end

  local function build_powder(surface, game)
    local amount_text = sol.text_surface.create({
      font = amount_font,
      horizontal_alignment = "right",
      vertical_alignment = "middle",
    })
    local background = sol.surface.create((grid_size * 2) + spacing, grid_size)
    
    background:fill_color(gray_color)
    background:set_opacity(opacity)
    background:draw(surface)
   
    local item_name = "powder"
    local item = game:get_item(item_name)
    local amount = item:get_amount() or 0  
    local origin_x, origin_y = item_sprite:get_origin()

    item_sprite:set_animation(item_name)
    item_sprite:draw(surface, origin_x + margin, 
      origin_y + margin)

    amount_text:set_text(amount)
    amount_text:draw(surface, grid_size + (spacing * 3), grid_size - spacing)
  end

  local function build_caption_box(surface, game)
    local width, height = surface:get_size()
    local background = sol.surface.create(width, height)

    background:fill_color(gray_color)
    background:set_opacity(opacity)
    background:draw(surface)
  end

  function pause_menu:on_started()
    
    self.surface = sol.surface.create(menu_width, menu_height)
    self.gems_surface = sol.surface.create((grid_size + spacing) * gem_columns, (grid_size + spacing) * gem_rows)
    self.misc_surface = sol.surface.create((grid_size + spacing) * 2, grid_size + spacing)
    self.powder_surface = sol.surface.create((grid_size + spacing) * 2, grid_size + spacing)
    self.caption_surface = sol.surface.create((grid_size + spacing) * 5 - spacing, grid_size + spacing)

    build_grid_background(self.gems_surface, game)
    build_misc(self.misc_surface, game)
    build_gems(self.gems_surface, game)
    build_powder(self.powder_surface, game)
    build_caption_box(self.caption_surface, game)

    update_cursor()
  end

  function pause_menu:on_draw(surface)    
    self.surface:clear()

    local x, y = menu_x, menu_y

    self.gems_surface:draw(self.surface, x, y)
    self.misc_surface:draw(self.surface, x + 140, y)
    self.powder_surface:draw(self.surface, x + 140, y + 28)
    self.caption_surface:draw(self.surface, caption_x - 12, caption_y)
    caption_text:draw(self.surface, caption_x + (spacing * 7), caption_y + 6)

    self.cursor:draw(self.surface, cursor_x, cursor_y)

    self.surface:draw(surface, x, y)
  end

  function pause_menu:on_command_pressed(command)
    
    local handled = false
    if command == "right" then
      move_cursor(1, 0)
      handled = true
    elseif command == "up" then
      move_cursor(0, -1)
      handled = true
    elseif command == "left" then
      move_cursor(-1, 0)
      handled = true
    elseif command == "down" then
      move_cursor(0, 1)
      handled = true
    elseif command == "action" then
      display_description()
      handled = true
    end

    return handled
  end

  -- Setup game pause and unpause events.
  game:register_event("on_paused", function(game)
    sol.menu.start(game, pause_menu)
  end)
  game:register_event("on_unpaused", function(game)
    sol.menu.stop(pause_menu)
  end)
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", setup_pause_menu)

return true