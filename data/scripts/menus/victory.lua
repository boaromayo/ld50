local language_manager = require("scripts/language_manager")

local victory_screen_builder = {}

function victory_screen_builder:new(game)

  local victory_screen = {}

  function victory_screen:on_started()
    
    self.mode = 1
    self.allow_input = false

    victory_screen.surface = sol.surface.create(sol.video.get_quest_size())

    victory_screen.background = sol.surface.create(sol.video.get_quest_size())

    local font, font_size = language_manager:get_dialog_font()
    local black_color = {0, 0, 0}
    local white_color = {255, 255, 255}
    local alignment = "center"

    victory_screen.background:fill_color(white_color)

    victory_screen.par_1_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })
    victory_screen.par_2_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    }) 
    victory_screen.par_3_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    }) 
    victory_screen.par_4_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })

    victory_screen.input_prompt_text = sol.text_surface.create({
      font = font, 
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })

    victory_screen.score_prompt_text = sol.text_surface.create({
      font = font,
      font_size = font_size,
      color = black_color,
      horizontal_alignment = alignment,
    })
    victory_screen.score_text = sol.text_surface.create({
      font = "square_digits",
      horizontal_alignment = alignment,
    })

    self:hide_components()
    self:set_text()

    sol.timer.start(self, 400, function()
      self.surface:fade_in(30)
      self:start()
    end)
  end

  function victory_screen:hide_components()
    self.background:set_opacity(0)

    self.par_1_text:set_opacity(0)
    self.par_2_text:set_opacity(0)
    self.par_3_text:set_opacity(0)
    self.par_4_text:set_opacity(0)

    self.score_prompt_text:set_opacity(0)
    self.score_text:set_opacity(0)

    self.input_prompt_text:set_opacity(0)
  end

  function victory_screen:set_text()

    self.par_1_text:set_text_key("victory.1")
    self.par_2_text:set_text_key("victory.2")
    self.par_3_text:set_text_key("victory.3")
    self.par_4_text:set_text_key("victory.4")

    self.score_prompt_text:set_text("Score: ")
    self.score_text:set_text(string.format("%3d", game:get_score()))

    self.input_prompt_text:set_text_key("title_screen.press_start")
  end

  function victory_screen:start()
    self.mode = 2

    victory_screen.background:fade_in(140)

    sol.timer.start(self, 1500, function()
      victory_screen.par_1_text:fade_in(70)
      sol.timer.start(self, 2000, function()
        victory_screen.par_2_text:fade_in(70)
        sol.timer.start(self, 2000, function()
          victory_screen.par_3_text:fade_in(70)
          sol.timer.start(self, 2000, function()
            victory_screen.par_4_text:fade_in(70)
          end)
        end)
      end)
    end)

    sol.timer.start(self, 9000, function()
      victory_screen.score_prompt_text:fade_in(70)
      victory_screen.score_text:fade_in(70)
      sol.timer.start(self, 2000, function()
        self.input_prompt_text:fade_in(60)
        self.allow_input = true
      end)
    end)
  end

  function victory_screen:on_draw(surface)
    local width, height = surface:get_size()

    self.background:draw(self.surface)

    if self.mode == 2 then
      self:draw_text(width)
      self:draw_score(width)
      self.input_prompt_text:draw(self.surface, width / 2, height - 20)
    end

    self.surface:draw(surface, width / 2 - 160, height / 2 - 120)
  end

  function victory_screen:draw_text(width)
    self.par_1_text:draw(self.surface, width / 2, 40)
    self.par_2_text:draw(self.surface, width / 2, 60)
    self.par_3_text:draw(self.surface, width / 2, 80)
    self.par_4_text:draw(self.surface, width / 2, 100)
  end  

  function victory_screen:draw_score(width)
    self.score_prompt_text:draw(self.surface, width / 2 - 30, 140)
    self.score_text:draw(self.surface, width / 2 + 30, 140)
  end

  function victory_screen:on_key_pressed(key)
    
    local handled = false

    if self.allow_input then
      if key == "escape" then
        sol.main.exit()
        handled = true
      elseif key == "space" or key == "return" then
        self:on_pre_finish()
        handled = true    
      end
    end

    return handled
  end

  function victory_screen:on_pre_finish()
  
    self.allow_input = false
    sol.audio.play_sound("confirm")
    self.input_prompt_text:fade_out(30)
    sol.timer.start(self, 1000, function()
      self.par_1_text:fade_out(30)
      sol.timer.start(self, 1000, function()
        self.par_2_text:fade_out(30)
        sol.timer.start(self, 1000, function()
          self.par_3_text:fade_out(30)
          sol.timer.start(self, 1000, function()
            self.par_4_text:fade_out(30)
            self.score_prompt_text:fade_out(50)
            self.score_text:fade_out(50)
            sol.timer.start(self, 2750, function()
              sol.menu.stop(self)
              sol.main.reset()
            end)
          end)
        end)
      end)
    end)
  end

  return victory_screen
end

return victory_screen_builder