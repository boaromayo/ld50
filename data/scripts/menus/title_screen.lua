local title_screen = {}

local language_manager = require("scripts/language_manager")

-- Set up global timer to set up fade out.
local timer = nil

function title_screen:on_started()  
  -- Create surface.
  self.surface = sol.surface.create(320, 240)
  -- Delay before fading into title.
  sol.timer.start(self, 500, function()
    self:setup_title()
  end)
end

function title_screen:setup_title()
  self.mode = 1
  self.allow_input = false
  -- Load title screen and other images.
  self.title_img = sol.surface.create("menus/title_screen.png")
  -- Get dialog font and color.
  local font, font_size = language_manager:get_dialog_font()
  local white_color = { 255, 255, 255 } -- White color.

  -- Setup copyright text.
  self.copyright_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    horizontal_alignment = "center",
    vertical_alignment = "bottom",
  })
  -- Setup version text.
  self.version_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    horizontal_alignment = "right",
  })
  -- Setup "press enter/spacebar" text.
  self.press_btn_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color, -- White color.
    horizontal_alignment = "center",
    vertical_alignment = "bottom"
  })
  -- Toggle for "press enter/spacebar" text.
  self.press_btn_loop = false
  self.press_btn_draw = false

  -- Set the text.
  self:set_text()

  timer = sol.timer.start(self, 1000, function()
    self.surface:fade_in(30)
    self:display_title()
  end)
end

function title_screen:display_title()
  self.mode = 2

  -- Start title scene processing.
  timer = sol.timer.start(self, 500, function()
    sol.audio.play_music("title")
    self.allow_input = true
  end)

  -- Function to toggle "press enter/spacebar" text.
  local function press_btn_toggle()
    if self.press_btn_loop then 
      self.press_btn_draw = not self.press_btn_draw
      sol.timer.start(self, 500, press_btn_toggle)
    end
  end
  -- Continue calling function.
  self.press_btn_loop = true
  sol.timer.start(self, 2000, press_btn_toggle)
end

-- Draw method.
function title_screen:on_draw(surface)

  self.surface:clear()

  -- Call drawing methods based on modes.
  if self.mode == 2 then
    self:on_draw_title()
  end

  -- Get size and draw to screen.
  local width, height = surface:get_size()
  self.surface:draw(surface, width / 2 - 160, height / 2 - 120)
end

function title_screen:set_text()
  
  -- Set information text.
  self.version_text:set_text("v" .. sol.main.get_quest_version())
  self.copyright_text:set_text_key("title_screen.copyright")
  self.press_btn_text:set_text_key("title_screen.press_start")
end

function title_screen:on_draw_title()

  -- Draw the components.
  self.title_img:draw(self.surface, 0, 0)
  if self.press_btn_draw then
    self.press_btn_text:draw(self.surface, 156, 236)
  end
  self.version_text:draw(self.surface, 27, 222)
  self.copyright_text:draw(self.surface, 44, 240)
end

-- Events called based on player keyboard input.
function title_screen:on_key_pressed(key)
  
  local handled = false

  -- Handle any key events.
  -- If ESC pressed, quit game.
  if self.allow_input then
    if key == "escape" then
      sol.main.exit()
      handled = true
    -- Otherwise, continue onto pre-finish.
    elseif key == "space" or key == "return" or key == "c" then
      -- Turn off button press prompt.
      self.press_btn_loop = false
      self.press_btn_draw = false
      -- Shut off global timer if it exists.
      if timer ~= nil then
        -- Stop and delete timer.
        timer:stop()
        timer = nil
        -- Switch modes.
        self:on_pre_finish()
      end
      handled = true
    end
  end
  
  return handled
end

local function fade_out_image(img, time)
  img:fade_out(time)
end

-- Pre-finish processing for title screen.
function title_screen:on_pre_finish()
  sol.audio.play_sound("confirm")
  sol.audio.stop_music()
  sol.timer.start(self, 500, function()
    fade_out_image(self.title_img, 30)
    fade_out_image(self.copyright_text, 30)
    fade_out_image(self.version_text, 30)
    sol.timer.start(self, 1000, function()
      sol.menu.stop(self)
    end)
  end)
end

return title_screen