-- Logo by boaromayo.

-- Create the boaromayo splash screen logo.

local boaromayo_logo = {}

-- Set up global timer to set up fade out on logo.
local timer = nil

-- Set up anti-skipper (?) to prevent multiple fade-outs.
local is_skipping = false

-- Delay constant.
local FADE_DELAY = 30

-- Start menu.
function boaromayo_logo:on_started()
  -- Create logo components.
  self.surface = sol.surface.create(sol.video.get_quest_size())
  self.logo = sol.surface.create("menus/boaromayo-splash.png")
  -- Create surface.
  self.surface:fill_color({0, 0, 0})
  self.surface:fade_in(FADE_DELAY)
  
  self:show_logo()
end

-- Show logo.
function boaromayo_logo:show_logo()
  -- Fade in logo.
  self.logo:fade_in(FADE_DELAY)

  -- Set global timer to wait before fading out logo.
  timer = sol.timer.start(self, 2000, function()
    self:on_pre_finish()
  end)
end

-- Draw method.
function boaromayo_logo:on_draw(screen)
  -- Get size of screen for blit drawing.
  local width, height = screen:get_size()
  self.logo:draw(self.surface)
  self.surface:draw(screen, width / 2 - 160, height / 2 - 120)
end

-- Events called when player presses a key.
function boaromayo_logo:on_key_pressed(key)

  local handled = false

  -- If escape key pressed, quit game.
  if key == "escape" then
    sol.main.exit()
    handled = true
  -- Handle input to fade out logo.
  elseif key == "space" or key == "return" then
    -- Shut off global timer if it exists and fade out logo.
    if timer ~= nil then
      -- Stop timer and delete timer.
      timer:stop()
      timer = nil
      -- Prepare to close menu.
      self:on_pre_finish()
    end
    handled = true
  end
  return handled -- Return to check if event was handled.
end

-- Fade out logo processing.
function boaromayo_logo:on_pre_finish()
  -- Prevent additional fade-outs if button pressed.
  if is_skipping then
    return
  end  

  -- Prevent additional fade-outs.
  is_skipping = true

  -- Fade out logo, then close menu after a second.
  self.surface:fade_out(FADE_DELAY)
  self.logo:fade_out(FADE_DELAY)

  sol.timer.start(self, 1000, function()
    sol.menu.stop(self)
  end)
end

return boaromayo_logo