-- Rock to mine.
local entity = ...
local game = entity:get_game()
local map = game:get_map()
local hero = map:get_hero()

local timer = nil

local random_item_drops = {
  pearl = 20,
  sapphire = 10,
  ruby = 8,
  emerald = 8,
  green_beryl = 5,
  topaz = 3,
  fuschia = 2,
  pyramid = 2,
  crystal = 2,
  powder = 1,
  vulkstone = 1
}

function entity:on_created()
  local sprite = entity:get_sprite() or entity:create_sprite("entities/rock")

  -- Rock entity properties.
  entity:set_traversable_by(false)
  entity.destroyed = false
  entity.life = tonumber(entity:get_property("hp")) or 3
  entity.material_name = entity:get_property("material")
  entity.material_variant = entity:get_property("material_variant") or 1

end

function entity:reacts(other)
  -- Check rock's durability or destroy it.
  entity.life = entity.life - hero:get_attack()
  if entity.life <= 0 then
    entity:break_rock()
  else
    entity:get_sprite():set_animation("hit")
    sol.audio.play_sound("hit")
    sol.timer.start(entity, 200, function()
      entity:get_sprite():set_animation("on_ground")
    end)
  end
end

function entity:break_rock()
  -- If it's being destroyed, don't do anything.
  if entity.destroyed then 
    return 
  end
  entity.destroyed = true

  local x, y, layer = entity:get_position()
  local treasure_name, treasure_variant
  -- Set item drop based on the rock's given material.
  if entity.material_name then
    treasure_name = entity.material_name
    treasure_variant = entity.material_variant
  else
    -- Or make a random material drop.
    treasure_name = entity:get_random_item()
    treasure_variant = entity.material_variant
  end
  -- Start item drop.
  map:create_pickable({
    x = x,
    y = y,
    layer = layer,
    treasure_name = treasure_name,
    treasure_variant = treasure_variant,
  })
  -- Crush rock animation.
  sol.audio.play_sound("rock_crush")
  entity:get_sprite():set_animation("destroy", function()
    entity:remove()
  end)
end

function entity:get_random_item()
  local rand = math.random(1, 100)
  local drop = nil
  for drop_type, drop_percentage in pairs(random_item_drops) do
    if rand <= drop_percentage then
      drop = drop_type
      break
    else
      rand = rand - drop_percentage
    end
  end
  return drop
end

function entity:on_interaction()
  game:start_dialog("_map.npc.rock.inspect")
end

return entity