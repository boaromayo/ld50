local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite

local enemies_touched = {}

function entity:on_created()

  entity:set_size(8, 8)
  entity:set_origin(4, 5)

  sprite = entity:get_sprite() or entity:create_sprite("entities/fireball")
  sprite:set_direction(entity:get_direction())

  function sprite:on_animation_finished()
    entity:remove()
  end

  sol.timer.start(entity, 1000, function()
    entity:remove()
  end)
end

entity:set_can_traverse("hero", true)
entity:set_can_traverse("crystal_block", true)
entity:set_can_traverse("stairs", true)
entity:set_can_traverse("teletransporter", true)
entity:set_can_traverse_ground("shallow_water", true)
entity:set_can_traverse_ground("deep_water", true)
entity:set_can_traverse_ground("grass", true)
entity:set_can_traverse_ground("hole", true)
entity:set_can_traverse_ground("prickles", true)
entity:set_can_traverse_ground("lava", true)
entity:set_can_traverse_ground("ice", true)

local function is_rock(destructible)
  local sprite = destructible:get_sprite()

  if sprite == nil then
    return false
  end

  local sprite_id = sprite:get_animation_set()
  return sprite_id == "entities/rock" or sprite_id:match("^entities/rock")
end

local function rock_collided(entity, other)
  if other:get_type() ~= "custom_entity" then
    return false
  end

  if not is_rock(other) then
    return false
  end

  local x, y, width, height = entity:get_bounding_box()
  return other:overlaps(x - 1, y - 1, width + 2, height + 2)
end

-- Ensure rocks are hit.
entity:add_collision_test(rock_collided, function(entity, other)
  
  if other:get_type() == "custom_entity" then
    if not is_rock(other) then
      return
    end
  
    local rock = other
    local rock_sprite = other:get_sprite()

    -- If rock is hit, this collision has been dealt with.
    if (is_rock(rock) and rock_sprite:get_animation() ~= "on_ground") then
      return
    end

    entity:stop_movement()
    if sprite ~= nil then
      sprite:set_animation("stopped")
    end
    
    if rock.reacts then
      rock:reacts(entity)
      entity:remove()
    end
  end
end)

-- Ensure enemies are hurt.
entity:add_collision_test("sprite", function(entity, other)

  if other:get_type() == "enemy" then
    local enemy = other
    if enemies_touched[enemy] then
      return
    end

    enemies_touched[enemy] = true
    local reaction = enemy:get_fire_reaction(enemy_sprite)
    enemy:receive_attack_consequence("fire", reaction)

    sol.timer.start(entity, 200, function()
      entity:remove()
    end)
  end
end)

return entity
