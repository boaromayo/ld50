local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
end

function chest_powder:on_opened()
  -- Once chest opened, brandish item.
  local name, variant, _ = chest_powder:get_treasure()
  hero:start_treasure(name, variant)

  -- Spawn enemies in room.
  sol.timer.start(self, 100, function()
    local enemies = map:get_entities("monster_trap")
    for enemy in enemies do
      enemy:set_enabled(true)
    end
  end)
end