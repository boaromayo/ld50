local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  -- Check beast door if fed, and open all entrance doors.
  map:check_beast_door()

  for i = 1, map:get_entities_count("door_entrance_") do
    map:set_doors_open("door_entrance_" .. i, true)
  end

  map.key_savegame_variable = "key_b3_obtained"
end

function map:check_beast_door()
  if game:get_value("door_beast_4_satisfied") then
    beast:set_enabled(false)
    beast_figure:set_enabled(false)
    map:set_doors_open("door_beast", true)
  else
    map:set_doors_open("door_beast", false)
  end
end

function sensor_to_b4:on_activated()
  hero:teleport("ld50/b4", "from_b3")
end

local function reveal_chest(block)
  local block_name = block:get_name()
  local chest
  local hero = map:get_hero()

  if block_name:match("^block_chest_") then
    -- Get the number from the block's name.
    local block_num = string.match(block_name, "%d+") 
    chest = map:get_entity("chest_b3_" .. tostring(block_num))

    hero:freeze()
    sol.timer.start(map, 300, function()
      chest:set_enabled(true)
      sol.audio.play_sound("treasure")
      sol.timer.start(map, 500, function()
        hero:unfreeze()
      end)
    end)
  end
end

function block_chest_1:on_moved()
  reveal_chest(block_chest_1)
end

function block_chest_2:on_moved()
  reveal_chest(block_chest_2)  
end

function block_chest_3:on_moved()
  reveal_chest(block_chest_3)
end

function block_chest_4:on_moved()
  reveal_chest(block_chest_4)
end

function beast:on_interaction()
  -- Have the player talk to the door beast if facing up.
  if hero:get_direction() == 1 then
    beast:talk()
  end
end

-- Door beast talk processing.
function beast:talk()
  local food = beast_figure:get_property("food")
  local food_item = game:get_item(food)
  local food_amount = tonumber(beast_figure:get_property("food_amount"))

  local food_caps = string.upper(food)
  local food_amount_str = tostring(food_amount)

  local beast_sprite = beast_figure:get_sprite()

  beast_sprite:set_ignore_suspend(true)
  beast_sprite:set_animation("talking")
  game:start_dialog("_map.npc.door_beast.1", function()

    if game:has_item(food) then
      if food_item:has_amount(food_amount) then
        game:start_dialog("_map.npc.door_beast.question", food, function(answer)
          if answer == 1 then
            food_item:remove_amount(food_amount)
            game:start_dialog("_map.npc.door_beast.3", function()
              hero:freeze()
              beast_sprite:set_animation("satisfied", function()
                beast_figure:set_visible(false)
              end)

              sol.timer.start(game, 3000, function()
                map:open_doors("door_beast")
                beast:set_enabled(false)
                beast_figure:set_enabled(false)
                sol.timer.start(game, 1000, function()
                  game:set_value("door_beast_4_satisfied", true)
                  hero:unfreeze()
                end)
              end)
            end)
          end
        end)
      else
        game:start_dialog("_map.npc.door_beast.not_enough", {food_amount_str, food_caps}, function()
          beast_sprite:set_animation("stopped")
        end)
      end 
    else
      game:start_dialog("_map.npc.door_beast.demand", {food_amount_str, food_caps}, function()
        beast_sprite:set_animation("stopped")
      end)
    end
  end)
end