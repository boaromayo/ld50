local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  -- Check beast door if fed, and open all entrance doors.
  map:check_beast_door()

  for i = 1, map:get_entities_count("door_entrance_") do
    map:set_doors_open("door_entrance_" .. i, true)
  end
  
  map.key_savegame_variable = "key_b1_obtained"
end

function map:check_beast_door()
  if game:get_value("door_beast_2_satisfied") then
    beast:set_enabled(false)
    beast_figure:set_enabled(false)
    map:set_doors_open("door_beast", true)
  else
    map:set_doors_open("door_beast", false)
  end
end

function beast:on_interaction()
  -- Have the player talk to the door beast if facing up.
  if hero:get_direction() == 1 then
    beast:talk()
  end
end

function sensor_to_b2:on_activated()
  local destination_num = math.random(1,2)
  hero:teleport("ld50/b2", "from_b1_" .. tostring(destination_num))
end

-- Door beast talk processing.
function beast:talk()
  local food = beast_figure:get_property("food")
  local food_item = game:get_item(food)
  local food_amount = tonumber(beast_figure:get_property("food_amount"))

  local food_caps = string.upper(food)
  local food_amount_str = tostring(food_amount)

  local beast_sprite = beast_figure:get_sprite()

  beast_sprite:set_ignore_suspend(true)
  beast_sprite:set_animation("talking")
  game:start_dialog("_map.npc.door_beast.1", function()

    if game:has_item(food) then
      if food_item:has_amount(food_amount) then
        game:start_dialog("_map.npc.door_beast.question", food, function(answer)
          if answer == 1 then
            food_item:remove_amount(food_amount)
            game:start_dialog("_map.npc.door_beast.2", function()
              hero:freeze()
              beast_sprite:set_animation("satisfied", function()
                beast_figure:set_visible(false)
              end)

              sol.timer.start(game, 3000, function()
                map:open_doors("door_beast")
                beast:set_enabled(false)
                beast_figure:set_enabled(false)
                sol.timer.start(game, 1000, function()
                  game:set_value("door_beast_2_satisfied", true)
                  hero:unfreeze()
                end)
              end)
            end)
          end
        end)
      else
        game:start_dialog("_map.npc.door_beast.not_enough", {food_amount_str, food_caps}, function()
          beast_sprite:set_animation("stopped")
        end)
      end 
    else
      game:start_dialog("_map.npc.door_beast.demand", {food_amount_str, food_caps}, function()
          beast_sprite:set_animation("stopped")
        end)
    end
  end)
end