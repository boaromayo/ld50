local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  -- Fed beast door should be open.
  map:check_beast_door()

  map.key_savegame_variable = "key_1f_obtained"
end

function map:on_opening_transition_finished()

  if game:get_value("tutorial") == nil then
    hero:freeze()
    game:stop_countdown()
    game:start_dialog("_map.start.scrap.1", function()
      game:start_dialog("_map.start.1", function()
        game:set_value("tutorial", true)
        hero:start_treasure("paper", 1)
        hero:unfreeze()
        game:start_countdown()
      end)
    end)
  end
end

function map:check_beast_door()
  if game:get_value("door_beast_1_satisfied") then
    beast:set_enabled(false)
    beast_figure:set_enabled(false)
    map:set_doors_open("door_beast", true)
  else
    map:set_doors_open("door_beast", false)
  end
end

function beast:on_interaction()
  -- Have the player talk to the door beast if facing up.
  if hero:get_direction() == 1 then
    beast:talk()
  end
end

-- Using a sensor instead of a teleporter because when activated,
-- destination is already set. Next map's destination must be set
-- at random when player touches sensor.
function sensor_to_b1:on_activated()
  local destination_num = math.random(1,2)
  hero:teleport("ld50/b1", "from_1f_" .. tostring(destination_num))
end

-- Door beast talk processing.
function beast:talk()
  local food = beast_figure:get_property("food") or "sapphire"
  local food_item = game:get_item(food)
  local food_amount = tonumber(beast_figure:get_property("food_amount")) or 2

  local food_caps = string.upper(food)
  local food_amount_str = tostring(food_amount)

  local beast_sprite = beast_figure:get_sprite()

  beast_sprite:set_ignore_suspend(true)
  beast_sprite:set_animation("talking")
  game:start_dialog("_map.npc.door_beast.1", function()

    if game:has_item(food) then
      if food_item:has_amount(food_amount) then
        game:start_dialog("_map.npc.door_beast.question", food, function(answer)
          if answer == 1 then
            food_item:remove_amount(food_amount)
            game:start_dialog("_map.npc.door_beast.2", function()
              hero:freeze()
              beast_sprite:set_animation("satisfied", function()
                beast_figure:set_visible(false)
              end)

              sol.timer.start(game, 3000, function()
                map:open_doors("door_beast")
                beast:set_enabled(false)
                beast_figure:set_enabled(false)
                sol.timer.start(game, 1000, function()
                  game:set_value("door_beast_1_satisfied", true)
                  hero:unfreeze()
                end)
              end)
            end)
          end
        end)
      else
        game:start_dialog("_map.npc.door_beast.not_enough", {food_amount_str, food_caps}, function()
          beast_sprite:set_animation("stopped")
        end)
      end 
    else
      game:start_dialog("_map.npc.door_beast.demand", {food_amount_str, food_caps}, function()
        beast_sprite:set_animation("stopped")
      end)
    end
  end)
end