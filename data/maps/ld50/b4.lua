local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

  game:stop_countdown()
  game:set_hud_enabled(false)
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
  local hero = map:get_hero()
  local camera = map:get_camera()
  local sensor = map:get_entity("sensor_victory")
  local movement = sol.movement.create("target")
  local cam_speed = 120

  hero:freeze()
  sol.timer.start(self, 100, function()
    movement:set_target(camera:get_position_to_track(sensor))
    movement:set_speed(cam_speed)
    movement:set_smooth(false)
    
    movement:start(camera, function()
      sol.timer.start(camera, 600, function()
        local movement_back = sol.movement.create("target")
        movement_back:set_target(camera:get_position_to_track(hero))
        movement_back:set_speed(cam_speed)
        movement_back:set_smooth(false)
        movement_back:start(camera, function()
          camera:start_tracking(hero)
          hero:unfreeze() 
        end)
      end)
    end)
  end)
end
