local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(3)
  enemy:set_damage(0)

  local width, height = sprite:get_size()
  enemy:set_size(width, height)
  enemy:set_origin(width / 2, height - 3)

  enemy.normal_speed = 16
  enemy.running_speed = 32

  enemy.detect_distance = 60
  
  enemy.awake_animation = "appearing"
  enemy.sleep_animation = "disappearing"

  enemy.awake = false
  enemy.pursuing_hero = false

  function sprite:on_animation_finished(animation)
    if animation == enemy.awake_animation then
      enemy:finish_wake_up()
    end
  end

  enemy:go_sleep()
end

function enemy:on_attacking_hero(hero)
  hero:start_hurt(enemy, 0)

  -- Take 10 seconds off player if hurt.
  if game:get_countdown_time() > 0 then
    game:take_countdown_time(10)
  end  
end

function enemy:on_movement_changed(movement)
  if enemy.awake then
    local direction = movement:get_direction4()
    sprite:set_direction(direction)
  end
end

function enemy:on_obstacle_reached(movement)
  if enemy.awake and not enemy.pursuing_hero then
    enemy:move_random()
    enemy:check_hero_nearby()
  end
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  if not enemy.awake then
    enemy:get_sprite():set_animation(enemy.sleep_animation)
  else
    enemy:move_random()
  end
  enemy:check_hero_nearby()
end

-- Check if hero is near enemy's range.
function enemy:check_hero_nearby()

  local near_hero

  -- Check if enemy sees hero nearby.
  local function is_nearby(enemy, hero)
    local hero_layer = hero:get_layer()
    local enemy_layer = enemy:get_layer()

    return (enemy_layer == hero_layer or enemy:has_layer_independent_collisions()) and (enemy:get_distance(hero) < enemy.detect_distance)
  end

  near_hero = is_nearby(enemy, hero) and enemy:is_in_same_region(hero)

  if enemy.awake and near_hero and not enemy.pursuing_hero then
    enemy:pursue()
  elseif enemy.awake and not near_hero and enemy.pursuing_hero then
    enemy:move_random()
  elseif not enemy.awake and near_hero then
    enemy:wake_up()
  end

  -- Stop enemy and do a small delay before changing behavior.
  sol.timer.stop_all(enemy)
  sol.timer.start(enemy, 500, function()
    enemy:check_hero_nearby()
  end)
end

-- Sleep enemy process.
function enemy:go_sleep()
  enemy:stop_movement()
  if enemy.sleep_animation ~= nil then
    sprite:set_animation(enemy.sleep_animation)
  end
end

-- Wake enemy up process.
function enemy:wake_up()
  enemy:stop_movement()
  enemy:finish_wake_up()
  if enemy.awake_animation ~= nil then
    sprite:set_animation(enemy.awake_animation)
  end
end

-- finish wakeup process.
function enemy:finish_wake_up()
  sprite:set_animation("walking")
  enemy.awake = true
  enemy:pursue()
end

-- Move randomly.
function enemy:move_random()
  enemy.pursuing_hero = false

  movement = sol.movement.create("random")
  movement:set_speed(enemy.normal_speed)
  movement:set_ignore_obstacles(false)
  movement:start(enemy)
end

-- Enemy sees hero and pursues.
function enemy:pursue()
  enemy.pursuing_hero = true

  movement = sol.movement.create("target")
  movement:set_speed(enemy.running_speed)
  movement:set_ignore_obstacles(false)
  movement:start(enemy)
end  