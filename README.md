# ld50

---

The source code for **Closed Circle**, for the **50th Ludum Dare Game Jam**, created using the free and open-source [Solarus Game Engine](https://www.solarus-games.org).

![img](https://static.jam.vg/raw/852/z/4c5a5.png)
![img2](https://static.jam.vg/raw/852/z/4c59a.png)

Trapped in a temple because of your curiosity (and your greed), get out of the temple before time runs out!

* **Players:** 1
* **License:** GPL v3, CC BY-SA 4.0
* **Languages:** English

## Controls

---

* **Arrow keys** - Move
* **Space bar** - Interact
* **C** - Use Weapon
* **D** - Pause
* **Escape** - Quit Game

## Tools

---

* [Aseprite](https://www.aseprite.org/) - pixel art
* [Solarus Quest Editor](https://www.solarus-games.org/) - game engine
* [bfxr](https://www.bfxr.net/) - sound effects
* [WolframTones](https://tones.wolfram.com) - intro music

## Installation

---

Download the zip file, unzip and click on the `ld50` executable to run the game!

### Solarus Launcher

You can download the `ld50.solarus` standalone, open the launcher (which you can download on the [Solarus game engine's website](https://solarus-games.org/en/solarus/download)), and click on `Add Quest > Find 'ld50.solarus' > Play` to run the game!

## Specifications

### Operating Systems

---

* Windows 7 or higher
* Mac OS 10.13.4 (High Sierra) or higher
* Linux Ubuntu 16.04 or higher

### Required Packages (Linux Only)

---

* OpenGL 3.0 or OpenGL ES
* SDL 2.0.6 or higher (2.0.10 **not recommended**)
* SDL2Main
* SDL2_image
* SDL2_ttf
* OpenAL
* Vorbisfile
* Ogg
* Modplug 0.8.4 or higher
* Lua 5.1 or LuaJIT 2.0 (**recommended**)
* Physfs
