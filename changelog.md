# Changelog

--

## v1.2.1 (2023-05-28)

* Fixed softlocking bugs in maps
* Door beast animation bugfix
* Doors stay open when door beasts fed
* Explain use of rod when retrieved
* Decreased amount of enemies spawning
* Tileset fixes
* Moved some HUD components to top of screen
* Ending screen font fix
* Some map touch-ups

## v1.2 (2022-05-28)

* Removed materials (stone, wood, and metal are gone!)
* Changed chance of item drops
* Fixed bug allowing player to move when tutorial dialogue is open
* Fixed enemy collision detection bugs (fireballs can now attack enemies!)
* Made ending cutscene camera pan faster
* Pause menu:
	* Powder counter now working
	* Items are removed if player has none in hand
	* Added item captions
* Minor title screen fixes
* Modified/fixed some areas
* Fixed upstairs transition bug: If player tries to go back a floor, they will be taken back to current floor
* Fixed countdown timer to turn back to white if above 100 seconds
* Moved score HUD
* Repositioned fireballs
* Added a little secret
* Improved audio (a tad bit!)

## Miscellany 3 (2022-04-26)

* Added standalone Solarus Launcher download

## Miscellany 2 (2022-04-22)

* Added separate Linux download (Ubuntu only, not tested with other distros)

## v1.1 (2022-04-21)

* Bugfixes
* New control mappings
* New weapon (Rod) added
* Introduction music added
* Pause menu added
* More floors for exploration (or misery)!
* A way to leave the temple

## Miscellany (2022-04-04)

* Added Windows executable

## v1.0.1 (2022-04-04)

This release fixes showstopping bugs post-submission.

* Fixed chests not yielding item
* Moved copyright text in title screen
* Repositioned score
