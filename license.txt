This file herein contains the license of this game, developed by boaromayo.

The code in this game herein is licensed under GNU General Public License version 3.

The Solarus Game Engine is licensed under the GNU General Public License version 3. For more information on the engine's source code, visit https://www.solarus-games.org/

Thank you to the Solarus Team and the community for some assistance in making this Ludum Dare entry possible.